1.Today we mainly conducted a code review, and then presented the concept map of the knowledge learned last week, as well as the learning of HTTP, Restful API, and Spring Boot:  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(1) In code reveiw, I found that some details of Code refactoring still need to be improved, such as some useless comments can be removed;  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(2) The concept map summarized the knowledge of oo, TDD, and Refactor, allowing me to review the knowledge I learned last week again;  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(3) HTTP and Restful API: As I have learned this knowledge before, I am quite familiar with this part of the knowledge.  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(4) SpringBoot: In the afternoon, we mainly had some exercises on SpringBoot, which gave us a certain understanding of using SpringBoot for web development.  
2.In the code review, I found that I still need to improve on some details of the refactor code, which makes me feel meaningful.  
3.I found that my understanding of concept maps is still insufficient, and my understanding of entities and relationships is not thorough enough.  
4.Through today's learning, I have become more familiar with the understanding of refactors and hope to apply them appropriately to practical development in the future.  