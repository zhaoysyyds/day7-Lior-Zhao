package com.thoughtworks.springbootemployee.repository;


import com.thoughtworks.springbootemployee.entity.Company;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Component
public class CompanyRepository {
    private final List<Company> companies = new ArrayList<>();


    public List<Company> getCompanies() {
        return companies;
    }

    public Company getCompanyById(long id) {
        return companies.stream().filter(company -> Objects.equals(company.getId(), id)).findFirst().orElse(null);
    }

    public List<Company> findCompaniesByPageAndSize(int page, int size) {
        return companies.stream()
                .skip((long) (page - 1) * size)
                .limit(size)
                .collect(Collectors.toList());
    }

    public Company save(Company company) {
        company.setId(generateId());
        companies.add(company);
        return company;
    }

    private Long generateId() {
        return companies.stream()
                .max(Comparator.comparingLong(Company::getId)).map(company -> company.getId() + 1).orElse(1L);
    }

    public Company update(long id, Company companyMessage) {
        return companies.stream().filter(company -> Objects.equals(company.getId(), id)).findFirst().map(company -> {
            company.setName(companyMessage.getName());
            return company;
        }).orElse(null);
    }

    public void delete(long id) {
        companies.removeIf(company -> Objects.equals(company.getId(), id));
    }
}
