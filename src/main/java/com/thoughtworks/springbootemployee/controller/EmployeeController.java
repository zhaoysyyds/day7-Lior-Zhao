package com.thoughtworks.springbootemployee.controller;

import com.thoughtworks.springbootemployee.entity.Employee;
import com.thoughtworks.springbootemployee.repository.EmployeeRepository;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/employees")
public class EmployeeController {
    @Resource
    private EmployeeRepository employeeRepository;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Employee createEmployee(@RequestBody Employee employee) {
        return employeeRepository.save(employee);
    }


    @GetMapping
    public List<Employee> getEmployees() {
        return employeeRepository.findAll();
    }


    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.CREATED)
    public Employee getEmployeeById(@PathVariable Long id) {
        return employeeRepository.findById(id);
    }


    @GetMapping(params = {"gender"})
    public List<Employee> findEmployees(@RequestParam String gender) {
        return employeeRepository.findByGender(gender);
    }


    @PutMapping(value = "/{id}")
    public Employee updateEmployee(@PathVariable Long id, @RequestBody Employee employeeMessage) {
        return employeeRepository.update(id, employeeMessage);
    }


    @DeleteMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteEmployee(@PathVariable Long id) {
        employeeRepository.deleteEmployee(id);
    }


    @GetMapping(params = {"page", "size"})
    public List<Employee> findEmployeesByPageAndSize(@RequestParam int page, @RequestParam int size) {
        if (page <= 0 || size <= 0) return null;
        return employeeRepository.findEmployeeByPageAndSize(page, size);
    }
}
